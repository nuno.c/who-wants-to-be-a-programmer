package org.academiadecodigo.strinteasers.quizclient.mvc.view;

public interface View {

    void show();
}
