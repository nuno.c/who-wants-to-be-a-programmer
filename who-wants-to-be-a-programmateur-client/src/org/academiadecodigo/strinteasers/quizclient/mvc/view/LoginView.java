package org.academiadecodigo.strinteasers.quizclient.mvc.view;


import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;
import org.academiadecodigo.strinteasers.quizclient.Client;
import org.academiadecodigo.strinteasers.quizclient.Message;
import org.academiadecodigo.strinteasers.quizclient.mvc.controller.LoginController;

public class LoginView extends AbstractView implements View {

    private LoginController loginController;
    private Client client;

    @Override
    public void show() {
        System.out.println(Message.WELCOME_MESSAGE);
        client.setHostAddress(askAndGetUserChoice(Message.ASK_HOST));
        client.setHostPort(setupPort(Message.ASK_PORT));
        client.start();
    }

    public void setLoginController(LoginController loginController) {
        this.loginController = loginController;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Integer setupPort(String message) {
        IntegerInputScanner integerInputScanner = new IntegerInputScanner();
        integerInputScanner.setMessage(message);
        return prompt.getUserInput(integerInputScanner);
    }
}

