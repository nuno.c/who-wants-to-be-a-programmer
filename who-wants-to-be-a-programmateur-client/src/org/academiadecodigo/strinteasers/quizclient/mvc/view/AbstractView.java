package org.academiadecodigo.strinteasers.quizclient.mvc.view;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;
import org.academiadecodigo.strinteasers.quizclient.Message;

public class AbstractView {

    protected Prompt prompt;

    public void setPrompt(Prompt prompt) {
        this.prompt = prompt;
    }

    public String askAndGetUserChoice(String message) {
        StringInputScanner stringInputScanner = new StringInputScanner();
        stringInputScanner.setMessage(message);
        return prompt.getUserInput(stringInputScanner);
    }


}
