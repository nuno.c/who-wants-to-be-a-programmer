package org.academiadecodigo.strinteasers.quizclient.mvc.controller;

import org.academiadecodigo.bootcamp.Prompt;

public class AbstractController {

    protected Prompt prompt;

    public void setPrompt(Prompt prompt) {
        this.prompt = prompt;
    }
}
