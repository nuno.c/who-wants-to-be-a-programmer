package org.academiadecodigo.strinteasers.quizclient.mvc.controller;

import org.academiadecodigo.strinteasers.quizclient.mvc.view.LoginView;

public class LoginController extends AbstractController implements Controller {

    private LoginView loginView;

    @Override
    public void init() {
        loginView.show();
    }

    public void setLoginView(LoginView loginView) {
        this.loginView = loginView;
    }


}
