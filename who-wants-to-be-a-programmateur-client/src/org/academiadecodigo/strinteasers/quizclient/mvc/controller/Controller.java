package org.academiadecodigo.strinteasers.quizclient.mvc.controller;

public interface Controller {

    void init();
}
