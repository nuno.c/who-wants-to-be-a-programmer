package org.academiadecodigo.strinteasers.quizclient;


import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

import java.io.*;
import java.net.Socket;

public class Client {

    private String hostAddress;
    private int port;

    private Socket clientSocket;
    private BufferedReader inputStream;
    private PrintWriter outputStream;

    private boolean connectionEstablished;

    private StringInputScanner stringInputScanner;
    private Prompt prompt;

    public Client() {
        stringInputScanner = new StringInputScanner();
    }

    public void start() {

        try {

            setupConnection();
            waitForEstablishedConnection();

            waitToStartGame();
            setMenu();



            waitForServerInput();   // -> HERE

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void waitToStartGame() throws IOException {

        String gameStarting = inputStream.readLine();
        while (!gameStarting.equals("Starting the game!")) {
            gameStarting = inputStream.readLine();
        }
        System.out.println(gameStarting);

    }


    public void setMenu() throws IOException {
        while(!clientSocket.isClosed()) {

            String question;
            question = inputStream.readLine();
            String[] options = new String[4];

            for (int i = 0; i < 4; i++) {
                options[i] = inputStream.readLine();    // -> only works for 4 possible answers
            }
            MenuInputScanner menuInputScanner = new MenuInputScanner(options);
            menuInputScanner.setMessage(question);
            outputStream.println(prompt.getUserInput(menuInputScanner));
            return;
        }
    }

    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    public void setHostPort(int port) {
        this.port = port;
    }

    public void setupStreams() throws IOException {

        inputStream = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        outputStream = new PrintWriter(clientSocket.getOutputStream(), true);
    }

    public void waitForServerInput() throws IOException  {
        while (!clientSocket.isClosed()) {

            System.out.println(inputStream.readLine());
        }
    }



    public void waitForEstablishedConnection() throws IOException {
        while (!connectionEstablished) {
            String serverLoginOutput = inputStream.readLine();

            if (serverLoginOutput.equals("Enter your username: ")) {
                connectionEstablished = true;
                stringInputScanner.setMessage(serverLoginOutput);
                String clientAnswer = prompt.getUserInput(stringInputScanner);
                outputStream.println(clientAnswer);
            }
            else {
                System.out.println(serverLoginOutput);
            }
        }
    }

    public void setupConnection() throws IOException {
        System.out.println(Message.CONNECT_TO_SERVER);
        clientSocket = new Socket(hostAddress, port);
        setupStreams();
    }

    public void setPrompt(Prompt prompt) {
        this.prompt = prompt;
    }

    public void sendToServer(String message) {
        outputStream.println(message);
    }


}
