package org.academiadecodigo.strinteasers.quizclient;

public class Message {
    private static final String LINE_BREAK = "\n";

    public static final String WELCOME_MESSAGE = "**## Welcome to Who Want´s To Be a Programmer ##**" + LINE_BREAK;
    public static final String USERNAME = "Enter your username: ";
    public static final String ASK_HOST = "Enter the host IP address: ";
    public static final String ASK_PORT = "Enter the host port: ";

    public static final String CONNECT_TO_SERVER = "Connecting to server..." + LINE_BREAK;
    public static final String CONNECTED_TO_SERVER = "Connection established to ";
}
