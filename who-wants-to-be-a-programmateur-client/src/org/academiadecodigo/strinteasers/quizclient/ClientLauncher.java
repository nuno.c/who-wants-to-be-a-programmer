package org.academiadecodigo.strinteasers.quizclient;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.strinteasers.quizclient.mvc.controller.LoginController;
import org.academiadecodigo.strinteasers.quizclient.mvc.view.LoginView;

public class ClientLauncher {

    public static void main(String[] args) {

        Client client = new Client();

        Prompt prompt = new Prompt(System.in, System.out);
        client.setPrompt(prompt);

        LoginController loginController = new LoginController();
        loginController.setPrompt(prompt);

        LoginView loginView = new LoginView();
        loginView.setPrompt(prompt);
        loginView.setClient(client);

        loginController.setLoginView(loginView);
        loginView.setLoginController(loginController);

        loginController.init();

    }

}
